# Changelog

<a name="v1.0.0"></a>
## [v1.0.0](https://0xacab.org/leap/obfsvpn/compare/v0.1.0...v1.0.0) (2024-05-14)

### Feature

* Unify hopping and regular API


### BREAKING CHANGE

This changes the manner in which bitmask/openvpn clients need to connect and the way in which bitmask openvpn servers must run. Instead of the "regular"/non-hopping PT using openvpn in TCP mode, both the hopping and non-hopping PT configurations need to use UDP mode.

The openvpn client [must be run with a remote specified instead of a socks proxy](https://0xacab.org/leap/obfsvpn/-/commit/09b1497c152ea5d52cb44ad42174f3ac9528bce8#fdfa9935cf5ed31d7e1155d44f72dec9870e5d38_39_37) and 
the [config must specify udp for the proto](https://0xacab.org/leap/obfsvpn/-/commit/09b1497c152ea5d52cb44ad42174f3ac9528bce8#8e8031bf471f30d718591b3348308ae806cc7a0a_3_3)

The openvpn server [also needs to change the proto to udp](https://0xacab.org/leap/obfsvpn/-/commit/09b1497c152ea5d52cb44ad42174f3ac9528bce8#708b8cab47f3e851eae0805b34783382bc81e165_3_2)
