#!/bin/bash

set -eo pipefail

mkdir -p /dev/net

if ! [ -c /dev/net/tun ]; then
    echo "$(date) Creating tun/tap device."
    mknod /dev/net/tun c 10 200
fi


FLAG_KCP=""
if [[ "$KCP" == "1" ]]; then
    FLAG_KCP="-kcp"
fi

if [[ "$HOP_PT" == "1" ]]; then

    echo "Starting obfsvpn-client in hopping mode"
    /usr/bin/obfsvpn-client $FLAG_KCP -h -c "$OBFS4_CERT1,$OBFS4_CERT1" -r "$OBFS4_SERVER_HOST1,$OBFS4_SERVER_HOST2" \
      -m "$MIN_HOP_SECONDS" -j "$HOP_JITTER" -v &

    # start openvpn in udp hopping mode
    # set connect-retry low to help facilitate integration test
    openvpn --config /vpn/client.ovpn --connect-retry 1 --remote 127.0.0.1 8080

elif [[ "$KCP" == "1" ]]; then
    echo "Starting obfsvpn-client in KCP mode"
    /usr/bin/obfsvpn-client -kcp -c "$OBFS4_CERT1" -r "$OBFS4_SERVER_HOST1" -rp "$OBFS4_PORT" -v &

    # start openvpn in traditional mode
    openvpn --config /vpn/client.ovpn --remote 127.0.0.1 8080
else
    # start the obfsvpn-client in traditional mode
    /usr/bin/obfsvpn-client -c "$OBFS4_CERT1" -r "$OBFS4_SERVER_HOST1" -rp "$OBFS4_PORT" -v &

    # start openvpn in traditional mode
    openvpn --config /vpn/client.ovpn --remote 127.0.0.1 8080
fi

exit $?

